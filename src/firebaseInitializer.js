import firebase from '@firebase/app';
const config = {
    apiKey: "AIzaSyCg97IgMpnPZ9pGhtoqgoHGHZzn-cERWXw",
    authDomain: "sigom-fca14.firebaseapp.com",
    databaseURL: "https://sigom-fca14.firebaseio.com",
    projectId: "sigom-fca14",
    storageBucket: "sigom-fca14.appspot.com",
    messagingSenderId: "362908606850",
    timestampsInSnapshots: true
}

if (!firebase.apps.length) {
    firebase.initializeApp(config)
}