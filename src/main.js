import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import { store } from './store'
import './registerServiceWorker'

import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import { initializeDatabase } from "@/firestore"
Vue.use(Vuetify)

Vue.config.productionTip = false

export const bus = new Vue()

export var usuario = null

initializeSecureApp()

let app;

function initializeSecureApp() {
  if (!app) {
    app = new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount('#app')
  }
}


initializeDatabase().then(function () {
  initializeSecureApp()
})


