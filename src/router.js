import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import OcorrenciaMariaDaPenha from '@/views/OcorrenciaMariaDaPenha'
import OcorrenciasTipo from "@/views/OcorrenciasTipo"
import OcorrenciaNaoFeita from "@/views/OcorrenciaNaoFeita"
import Login from "@/views/Login"
Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/home',
      name: 'home',
      component: Home
    },
    {
      path: '/',
      name: 'login',
      component: Login
    },
    {
      path: '/ocorrencias/:departamento',
      name: 'ocorrenciasdepartamento',
      component: OcorrenciasTipo,
    },
    {
      path: '/cad/ocorrenciamulher/:protocolo',
      name: 'ocorrenciamulher',
      component: OcorrenciaMariaDaPenha
    },

    {
      path: '/cad/ocorrenciaambiental/:protocolo',
      name: 'ocorrenciambiental',
      component: OcorrenciaNaoFeita
    },

    {
      path: '/cad/ocorrenciaambiental/:protocolo',
      name: 'ocorrenciaambiental',
      component: OcorrenciaNaoFeita
    },

    {
      path: '/cad/ocorrenciadepredacao/:protocolo',
      name: 'ocorrenciadepredacao',
      component: OcorrenciaNaoFeita
    },

    {
      path: '/cad/ocorrenciasilencio/:protocolo',
      name: 'ocorrenciasilencio',
      component: OcorrenciaNaoFeita
    },



    {
      path: '/cad/outrasocorrencias/:protocolo',
      name: 'outrasocorrencias',
      component: OcorrenciaNaoFeita
    },


    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    }
  ]
})
