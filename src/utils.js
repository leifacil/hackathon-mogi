export function formattedDate(unix_timestamp) {
    var d = new Date(unix_timestamp * 1000)
    let month = String(d.getMonth() + 1);
    let day = String(d.getDate());
    const year = String(d.getFullYear());

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return `${day}/${month}/${year}`;
}

export function formattedTime(unix_timestamp) {


    var date = new Date(unix_timestamp * 1000);



    // Hours part from the timestamp
    var hours = "0";

    if (date.getHours() < 10) {
        hours = hours.concat(date.getHours())
    }
    // Minutes part from the timestamp
    var minutes = "0" + date.getMinutes();
    // Seconds part from the timestamp
    var seconds = "0" + date.getSeconds();

    // Will display time in 10:30:23 format
    var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
    return formattedTime
}

var departamentos = {
    "oco-1": "Patrulha da Maria da Penha",
    "oco-2": "Crime Ambiental",
    "oco-3": "Depredação de Patrimônio",
    "oco-4": "Lei do Silêncio"
}

export function getIdFromDepartamento(departamento) {
    return new Map([...departamento].reverse())[departamento]
}

export function getDepartamentoFromId(id) {
    return departamentos[id]
}