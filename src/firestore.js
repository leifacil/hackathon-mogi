// /*eslint-disable no-unused-vars*/


import '@/firebaseInitializer'
import * as firebase from '@firebase/app'
import '@firebase/firestore'

export var db = firebase.firebase.firestore()
const settings = { timestampsInSnapshots: true }
db.settings(settings)

export function initializeDatabase() {
    return new Promise((resolve, _) => {
        firebase.firebase.firestore().enablePersistence({ experimentalTabSynchronization: true })
            .then(function () {

                //update the database variable to reflect the one with persistenceEnabled
                db = firebase.firestore();

                resolve()
            })
            .catch(function (err) {
                if (err.code == 'failed-precondition') {
                    console.log(err)
                    // Multiple tabs open, persistence can only be enabled
                    // in one tab at a a time.
                    // ...
                } else if (err.code == 'unimplemented') {
                    console.log(err)
                    // The current browser does not support all of the
                    // features required to enable persistence
                    // ...
                }

                //Nothing to do, it was previsoly initilaized in the var db 
                resolve()
            });
    })
}



export var ocorrenciasCollection = db.collection("ocorrencias")
export function salvarOcorrencia(ocorrencia) {
    console.log(ocorrencia)
    ocorrenciasCollection
        .doc(ocorrencia.protocolo)
        .set(ocorrencia, { merge: true }).then(() => { })
}

