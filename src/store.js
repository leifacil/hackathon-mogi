import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

var state = {
  usuarioLogado: null
}
var actions = {
  setUsuario: ({ commit, state }, newValue) => {
    commit('SET_USUARIO', newValue)
    return state.usuario
  },
}
var mutations = {
  SET_USUARIO: (state, newValue) => {
    state.usuarioLogado = newValue
  },
}

var getters = {
  usuario() {
    return state.usuarioLogado
  }
}

export var store = new Vuex.Store({
  state: state,
  actions: actions,
  mutations: mutations,
  getters: getters
})
